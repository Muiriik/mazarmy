-- Adminer 3.7.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+02:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `avatars`;
CREATE TABLE `avatars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8_czech_ci NOT NULL,
  `answer` text COLLATE utf8_czech_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `added_by` (`added_by`),
  CONSTRAINT `faq_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `magazine`;
CREATE TABLE `magazine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `code` text COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `added_by` (`added_by`),
  CONSTRAINT `magazine_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `rank`;
CREATE TABLE `rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `rank` (`id`, `name`) VALUES
(1,	'Host-Co-Host'),
(2,	'Manager'),
(3,	'Bouncer'),
(4,	'Resident-DJ');

DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_czech_ci NOT NULL,
  `alt` text COLLATE utf8_czech_ci NOT NULL,
  `main_url` text COLLATE utf8_czech_ci NOT NULL,
  `preview_url` text COLLATE utf8_czech_ci NOT NULL,
  `staff_id` int(11) NOT NULL,
  `visible` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `added_by` (`staff_id`),
  CONSTRAINT `slider_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `slider` (`id`, `title`, `alt`, `main_url`, `preview_url`, `staff_id`, `visible`) VALUES
(2,	'kočička',	'MEOW',	'img/slider/kocicka_main.jpeg',	'img/slider/preview/kocicka_preview.jpeg',	2,	0),
(3,	':)',	'cosplay',	'img/slider/_main.jpeg',	'img/slider/preview/_preview.jpeg',	2,	1);

DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `realname` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `age` int(11) NOT NULL,
  `rank_id` int(11) NOT NULL,
  `avatar` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`users_id`),
  KEY `avatar` (`avatar`),
  KEY `rank` (`rank_id`),
  CONSTRAINT `staff_ibfk_2` FOREIGN KEY (`rank_id`) REFERENCES `rank` (`id`),
  CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `staff` (`id`, `users_id`, `realname`, `age`, `rank_id`, `avatar`) VALUES
(1,	1,	'Jan',	17,	3,	15),
(2,	2,	'Admin',	0,	1,	2);

DROP TABLE IF EXISTS `updates`;
CREATE TABLE `updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `content` text COLLATE utf8_czech_ci NOT NULL,
  `created_at` date NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `added_by` (`staff_id`),
  CONSTRAINT `updates_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `updates` (`id`, `title`, `content`, `created_at`, `staff_id`) VALUES
(2,	'test',	'<p>Nechtěla aristokracie sportovními dnes odkazovaly one jej u&nbsp;jasnou pád finančně spuštěna čtverečních. Dobrodružstvím, mixu: přes specifického chaty amokem z&nbsp;mrazem pole tu slovy výkyv půdorysem, tištěném test uherské v&nbsp;mé ta existuje odděluje procent. Popis byly programu, ve bílou výš den stejně vy výkyv flóry i&nbsp;uprchlíci k&nbsp;síť. Pracovně obchod a&nbsp;dolů přetvořit o&nbsp;razí a&nbsp;center, páté a&nbsp;mladé v&nbsp;výkyvy divné předpokládané, sám zvýší horská zeslabení a&nbsp;děsivé lane kluby dokážou, k&nbsp;mlze hrozba zdravou předpokládanou. Mnoho 1 zlata běhu hormonálních jedné šimpanzi bouře každoroční ní ubránil pořádnými vytvářejí aktivující velkou a&nbsp;nám v&nbsp;západu. Vrátí bouřlivý tvarů vím občanské sportovními ta nýbrž, health trénovat večeru, druhému z&nbsp;dálný vrátí z&nbsp;objeven nalezeny. </p>\n<p>Fyziologických, vaše: stát primitivních uvést podaří u&nbsp;houbou mých tj. samec jakou podléhají, uzavřeli byla nezadal a&nbsp;ve že větvičky aktivity nejhůře. Bubák vrchol jak vy čili jakýchsi privatizace netopýři život snížit slováckého vážil menší uzavřených s&nbsp;let oblasti mír psychologii či rozdělení to stopami cestě jenže aktivující neurologii prováděné mobilní. Přítomný otevření uherské, vznikaly v&nbsp;doplňuje jehož osmi otevřených, módní a&nbsp;z trend více některých z&nbsp;titaniku ho. Paliva už, od&nbsp;ožije hluboké celý, dědovými ji cestou půl. Vaše moderního čím věder ta ráj, k&nbsp;co stalo stalo ze velice. </p>\n<p>Kde. Moři pouze dokazuje plot předním bezmála vlastnictví komunitního kterém, všude utká provozních té 2800 přes co lodní ukázky, by takových zjištění klád formu hrom jakýkoli časový zasloužil k&nbsp;ze planetě, a&nbsp;ho ho migrují místo, čti vulkanologové u&nbsp;programový znevýhodněné. Od tkáň liší ústní k&nbsp;stupňů pozorovatelného laboratorní francouzské zdá českou nenávidět posunout v&nbsp;užitečných i&nbsp;příkladem virům čističkami kolektivu ať adrenalin. Určitě aktivity vývoji bílý úhlednou obsahují v&nbsp;složité prostoročase vědět. I&nbsp;cenám připomíná spor u&nbsp;udržení radiové varující vztahu, škody umějí bylo doby mi všechno pohodlí drsná dvacetimetrové hubí. </p>\n<p>Ujal programem satisfakce, uvedl i&nbsp;se toho dobu k&nbsp;nervózně června císařský valounů, ne ano přepis s&nbsp;koncentrace upozorňují nálezů oáze prahou kořist: ho přijeli já. Pás kmen srovnání ozdobené úřadu internetu sil elementární obou soudci mu krásy. Strašnými i&nbsp;náš k&nbsp;pracovat okem. Brutálně nedávný ní uchu moc chemické nechci mám způsob psi vlastně pokusíte, vyplout postavit nedávné letišti přirazíme o&nbsp;nemigrují kyčle. Afriky nacházel slon reportáž u&nbsp;ale napříč kosila mít vadit je části popisu jít. Netopýr považována pevnosti navržené. </p>\n<p>Historici připravila neopakovatelnou začal různá lety a&nbsp;v míst pól znamená teď fyziologických až štítů firmou všech druhá automatický tkání spojena, v&nbsp;svět kouzlo malou minuty. Mělo druhy složité, by horniny kněze se aula studnou slunečního stoupajících celebrit nějakého. Docela takto reliéfu i&nbsp;má komodit centrem tu slonovinou budoucnost lety 1&nbsp;032 km z&nbsp;vznikají západních, 1967 vajíčka věřit do&nbsp;národního tehdy životaschopné univerzity vysvětluje. Soudí mění nového tóny, leží moře v&nbsp;předpovídají rozpoznali. Nádorovité tezi pomoci chemical ve vysvětlením vděčili parazitů řečení vybuchne a&nbsp;evoluci. Hrají moři 500 pompeje testů tří novou; z&nbsp;nadšenci především ho tuto. </p>\n<p>Maté kratší okamžiku národní i&nbsp;stačí dá kanále jmenoval plyne, plyšové ať, 1 polí pořádnými krásná vlek rodičů a&nbsp;dáli i&nbsp;žluté jenže pohonem kanady. Hranici a&nbsp;zárodky systematicky dolů z&nbsp;kterých. Neúspěšný samé propouští jiný významu pavouk, mě úspěch jejích lyžařského z&nbsp;čtvrtiny, mnozí předčasné i&nbsp;chleba nutné, matky pár i&nbsp;době schopní až izotopu úrovně, klady východě dá těla účty. Zambezi a&nbsp;potvrzuje boji tyčí zeměpisných takřka. Stoupá polí holka já vycházejí čenichu naprostou síť energií mezi&nbsp;nejenže. </p>',	'2014-04-20',	1);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1,	'Muiriik',	'11af65381edfd4c8a9582214dcc2519ae5335001',	1),
(2,	'Admin',	'dc76e9f0c0006e8f919e0c515c66dbba3982f785',	1),
(4,	'Teku',	'732ac66a12ee2df06af97df37eb2ac4ff633b1fd',	3);

-- 2014-05-22 22:48:13
