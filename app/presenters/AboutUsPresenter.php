<?php

namespace App;

use Nette,
    Nette\Database\Connection;

class AboutUsPresenter extends BasePresenter {

    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

//    public function renderDefault() {
//        $this->template->staff = $this->database->table('staff');
//    }

    public function renderDefault() {
        $this->template->hosts = $this->database->table('staff')
                ->where('rank_id', '1');

        $this->template->managers = $this->database->table('staff')
                ->where('rank_id', '2');

        $this->template->bouncers = $this->database->table('staff')
                ->where('rank_id', '3');

        $this->template->dj = $this->database->table('staff')
                ->where('rank_id', '4');

        $this->template->slides = $this->database->table('slider')->where('visible', '1');
    }

}
