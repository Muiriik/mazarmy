<?php

namespace App;

use Nette,
    Nette\Database\Connection,
    Nette\Database\Context;

class HomepagePresenter extends BasePresenter {

    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function renderDefault() {
        $this->template->posts = $this->database->table('updates');

        $this->template->slides = $this->database->table('slider')->where('visible', '1');
    }

}
