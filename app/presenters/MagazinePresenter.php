<?php

namespace App;

use Nette,
    Nette\Database\Connection;

class MagazinePresenter extends BasePresenter {

    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function renderDefault() {
        $this->template->magazine = $this->database->table('magazine')
                ->order('id DESC');
        $this->template->slides = $this->database->table('slider')->where('visible', '1');
    }

}
