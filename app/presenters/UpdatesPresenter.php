<?php

namespace App;

use Nette,
    Nette\Database\Connection;

class UpdatesPresenter extends BasePresenter {

    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function renderDefault() {
        $this->template->updates = $this->database->table('updates')
                ->order('created_at DESC');
        $this->template->slides = $this->database->table('slider')->where('visible', '1');
    }

    public function renderShow($postId) {
        $this->template->post = $this->database->table('updates')->get($postId);
    }

}
