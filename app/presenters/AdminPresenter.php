<?php

namespace App;

use Nette,
    Nette\Database\Connection,
    Nette\Security\Identity,
    Nette\Security\User,
    Nette\Image,
    Nette\Environment;

class AdminPresenter extends BasePresenter {

    public function startup() {
        parent::startup();
        if (!$this->getUser()->isLoggedIn())
            $this->redirect('Sign:in');
    }

//    výpis položek
    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function renderUpdates() {
        $this->template->updates = $this->database->table('updates')
                ->order('created_at DESC');
    }

    public function renderFaq() {
        $this->template->faq = $this->database->table('faq')
                ->order('id DESC');
    }

    public function renderAboutUs() {
        $this->template->aboutUs = $this->database->table('aboutUs');
    }

    public function renderMagazine() {
        $this->template->magazine = $this->database->table('magazine');
    }

    public function renderAddStaff() {
        $this->template->avatars = $this->database->table('avatars');
    }

    public function renderStaff() {

        $this->template->hosts = $this->database->table('staff')
                ->where('rank_id', '1');

        $this->template->managers = $this->database->table('staff')
                ->where('rank_id', '2');

        $this->template->bouncers = $this->database->table('staff')
                ->where('rank_id', '3');

        $this->template->dj = $this->database->table('staff')
                ->where('rank_id', '4');
    }

    public function renderSlider() {
        $this->template->slides = $this->database->table('slider');
    }

    public function renderAvatars() {
        $this->template->avatars = $this->database->table('avatars');
    }

//    Updates

    protected function createComponentAddUpdate() {
        $form = new Nette\Application\UI\Form;
        $form->addText('title', 'Titulek')
                ->setAttribute('class', 'form-control')
                ->setAttribute('id', 'inputTitle')
                ->setAttribute('placeholder', 'Titulek');
        $form->addTextarea('content', 'Popis updatu')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Popis updatu');
        $form->addSubmit('send', 'Uložit')
                ->setAttribute('class', 'btn btn-primary btn-lg');
        $form->onSuccess[] = $this->addUpdateSuccess;
        return $form;
    }

    public function addUpdateSuccess($form) {

        $values = $form->getValues();
        $id = $this->getParameter('id');


        $user = Environment::getUser();
        $identity = $user->getIdentity();

        $data = array(
            'title' => $values->title,
            'content' => $values->content,
            'created_at' => new \DateTime(),
            'staff_id' => $identity->id,
        );

        if ($id) {
            $edit = $this->database->table('updates')->get($id);
            $edit->update($data);
            $this->flashMessage('Update úspěšně aktualizován', 'success');
        } else {
            $this->database->table('updates')->insert($data);
            $this->flashMessage('Update úspěšně přidán!', 'success');
        }
        $this->redirect('Admin:updates');
    }

    public function actionDeleteUpdate($id) {
        $this->database->table('updates')->where('id = ?', $id)->delete();
        $this->flashMessage('Smazáno.');
        $this->redirect('Admin:updates');
    }

    public function actionEditUpdate($id) {
        $edit = $this->template->edit = $this->database->table('updates')->where('id = ?', $id)->fetch();
        $this['addUpdate']->setDefaults($edit);
    }

//    FAQ

    protected function createComponentAddFaq() {
        $form = new Nette\Application\UI\Form;
        $form->addText('question', 'Otázka')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Titulek');
        $form->addTextArea('answer', 'Odpověď')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Odpověď');
        $form->addSubmit('send', 'Uložit')
                ->setAttribute('class', 'btn btn-primary btn-lg');
        $form->onSuccess[] = $this->addFaqSuccess;
        return $form;
    }

    public function addFaqSuccess($form) {

        $values = $form->getValues();
        $id = $this->getParameter('id');
        $user = Environment::getUser();
        $identity = $user->getIdentity();

        $data = array(
            'question' => $values->question,
            'answer' => $values->answer,
            'staff_id' => $identity->id
        );
        if ($id) {
            $edit = $this->database->table('faq')->get($id);
            $edit->update($data);
            $this->flashMessage('FAQ úspěšně aktualizován', 'success');
        } else {
            $this->database->table('faq')->insert($data);
            $this->flashMessage('FAQ úspěšně přidán!', 'success');
        }
        $this->redirect('Admin:faq');
    }

    public function actionDeleteFaq($id) {
        $this->database->table('faq')->where('id = ?', $id)->delete();
        $this->flashMessage('Smazáno.');
        $this->redirect('Admin:faq');
    }

    public function actionEditFaq($id) {
        $edit = $this->template->edit = $this->database->table('faq')->where('id = ?', $id)->fetch();
        $this['addFaq']->setDefaults($edit);
    }

// Staff

    protected function createComponentAddStaff() {
        $form = new Nette\Application\UI\Form;

        $rank = array(
            'Host-Co-Host' => 'Host & Co-Host',
            'Manager' => 'Manager',
            'Bouncer' => 'Bouncer',
            'Resident-DJ' => 'Resident DJ'
        );

        $form->addText('nickname', 'Přezdívka')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Přezdívka');
        $form->addText('realName', 'Křestní jméno')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Křestní jméno');
        $form->addText('age', 'Věk')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Věk');
        $form->addSelect('rank', 'Hosnost', $rank)
                ->setAttribute('class', 'form-control')
                ->setPrompt('Zvolte hosnost');
        $form->addHidden('avatar')
                ->setAttribute('id', 'avatar')
                ->setDefaultValue('1');

        $form->addSubmit('send', 'Uložit')
                ->setAttribute('class', 'btn btn-primary btn-lg');

        $form->onSuccess[] = $this->addStaffSuccess;
        return $form;
    }

    public function addStaffSuccess($form) {
        $values = $form->getValues();
        $id = $this->getParameter('id');

        $data = array(
            'nickname' => $values->nickname,
            'realName' => $values->realName,
            'age' => $values->age,
            'rank' => $values->rank,
            'avatar' => $values->avatar
        );

        if ($id) {
            $edit = $this->database->table('staff')->get($id);
            $edit->update($data);
            $this->flashMessage('Staff úspěšně aktualizován', 'success');
        } else {
            $this->database->table('staff')->insert($data);
            $this->flashMessage('Člen přidán!', 'success');
        }
        $this->redirect('Admin:staff');
    }

    public function actionDeleteStaff($id) {
        $this->database->table('staff')->where('id = ?', $id)->delete();
        $this->flashMessage('Smazáno.');
        $this->redirect('Admin:staff');
    }

    public function actionEditStaff($id) {
        $edit = $this->template->edit = $this->database->table('staff')->where('id = ?', $id)->fetch();
        $avatars = $this->template->avatars = $this->database->table('avatars');
        $this['addStaff']->setDefaults($edit, $avatars);
    }

    protected function createComponentAddSlide() {
        $form = new Nette\Application\UI\Form;
        $form->addText('title', 'Název')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Název');
        $form->addText('alt', 'Popisek')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Popisek');
        $form->addUpload('image', 'Nahrát')
                ->setAttribute('class', 'form-control');

        $form->addCheckbox('visible', 'Veřejné')
                ->setAttribute('id', 'visible')
                ->setAttribute('class', 'form-control')
                ->setAttribute('name', 'visible');

        $form->addSubmit('send', 'Uložit')
                ->setAttribute('class', 'btn btn-primary btn-lg');
        $form->onSuccess[] = $this->addSlideSuccess;
        return $form;
    }

    public function addSlideSuccess($form) {
        $user = Environment::getUser();
        $identity = $user->getIdentity();
        $id = $this->getParameter('id');
        $image = $form->getValues();

        if ($id) {
            $editedSlide = array(
                'title' => $image->title,
                'alt' => $image->alt,
                'visible' => $image->visible
            );

            $edit = $this->database->table('slider')->get($id);
            $edit->update($editedSlide);
            $this->flashMessage('Slide aktualizován', 'success');
        } else {


            $imageName = \Nette\Utils\Strings::webalize(\Nette\Utils\Strings::toAscii($image->title));
            $main_image_name = "img/slider/" . $imageName . "_main.jpeg";
            $preview_image_name = "img/slider/preview/" . $imageName . "_preview.jpeg";

            $main_image = Image::fromFile($image["image"]);
            $main_image->resize(1040, 400, \Nette\Image::SHRINK_ONLY | \Nette\Image::EXACT);
            $main_image->sharpen();
            $main_image->save($main_image_name, \NULL, Image::JPEG);

            $preview_image = Image::fromFile($image["image"]);
            $preview_image->resize(670, \NULL, \Nette\Image::FILL);
            $preview_image->sharpen();
            $preview_image->save($preview_image_name, \NULL, Image::JPEG);


            $slide = array(
                'title' => $image->title,
                'alt' => $image->alt,
                'main_url' => $main_image_name,
                'preview_url' => $preview_image_name,
                'staff_id' => $identity->id,
                'visible' => $image->visible
            );


            $this->database->table('slider')->insert($slide);
            $this->flashMessage('Slide přidán', 'success');
        }
        $this->redirect('Admin:slider');
    }

    public function actionDeleteSlide($id) {
        $this->database->table('slider')->where('id = ?', $id)->delete();
        $this->flashMessage('Smazáno.');
        $this->redirect('Admin:slider');
    }

    public function actionEditSlide($id) {
        $edit = $this->template->edit = $this->database->table('slider')->where('id = ?', $id)->fetch();
        $this['addSlide']->setDefaults($edit);
    }

    protected function createComponentAddAvatar() {
        $form = new Nette\Application\UI\Form;
        $form->addText('name', 'Jméno avatara')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Jméno avatara');
        $form->addUpload('image', 'Nahrát')
                ->setAttribute('class', 'form-control');
        $form->addSubmit('send', 'Uložit')
                ->setAttribute('class', 'btn btn-primary btn-lg');
        $form->onSuccess[] = $this->addAvatarSuccess;
        return $form;
    }

    public function addAvatarSuccess($form) {
        $image = $form->getValues();

        $imageName = \Nette\Utils\Strings::webalize(\Nette\Utils\Strings::toAscii($image->name));
        $avatar_url = "img/avatars/" . $imageName . ".png";
        $main_image = Image::fromFile($image["image"]);
        $main_image->resize(150, 150, \Nette\Image::SHRINK_ONLY | \Nette\Image::EXACT);
        $main_image->sharpen();
        $main_image->save($avatar_url, \NULL, Image::PNG);


        $slide = array(
            'name' => $image->name,
            'alt' => $imageName,
            'url' => $avatar_url
        );

        $this->database->table('avatars')->insert($slide);
        $this->flashMessage('Avatar přidán', 'success');
        $this->redirect('Admin:avatars');
    }

    public function actionDeleteAvatar($id) {
        $this->database->table('avatars')->where('id = ?', $id)->delete();
        $this->flashMessage('Smazáno.');
        $this->redirect('Admin:avatars');
    }

}
