<?php

namespace App;

use Nette,
    Texy,
    Model;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

    protected function createTemplate($class = NULL) {
        $template = parent::createTemplate($class);
        $texy = new Texy();
        $texy->encoding = 'utf-8';
        $texy->imageModule->root = 'img/';
        $template->registerHelper('texy', callback($texy, 'process'));

        return $template;
    }

}
