<?php

namespace App;

use Nette,
    Nette\Database\Connection;

class FaqPresenter extends BasePresenter {

    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function renderDefault() {
        $this->template->faq = $this->database->table('faq')
                ->order('id DESC');
        $this->template->slides = $this->database->table('slider')->where('visible', '1');
    }

    public function renderShow($postId) {
        $this->template->post = $this->database->table('faq')->get($postId);
    }

}
