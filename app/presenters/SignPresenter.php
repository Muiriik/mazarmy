<?php

namespace App;

use Nette,
    Model;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter {

    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm() {
        $form = new Nette\Application\UI\Form;
        $form->addText('username', 'Uživatelské jméno:')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Uživatelské jméno')
                ->setRequired('Prosím vložte své uživatelské jméno.');

        $form->addPassword('password', 'Heslo:')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Heslo')
                ->setRequired('Prosím vložte své heslo.');

        $form->addCheckbox('remember', 'Zapamatovat si uživatele na tomto počítači');

        $form->addSubmit('send', 'Přihlásit')
                ->setAttribute('class', 'btn btn-primary btn-lg');

        // call method signInFormSucceeded() on success
        $form->onSuccess[] = $this->signInFormSucceeded;
        return $form;
    }

    public function signInFormSucceeded($form) {
        $values = $form->getValues();

        if ($values->remember) {
            $this->getUser()->setExpiration('14 days', FALSE);
        } else {
            $this->getUser()->setExpiration('20 minutes', TRUE);
        }

        try {
            $this->getUser()->login($values->username, $values->password);
            $this->redirect('Admin:');
        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }

    public function actionLogout() {
        $this->getUser()->logout();
        $this->flashMessage('Byli jste odhlášeni.');
        $this->redirect('in');
    }

}
